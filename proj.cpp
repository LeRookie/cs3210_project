#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <vector>
#include <string>

#define MAX_SIZE 32
#define EMPTY 0
#define QUEEN 1
#define TRUE 1
#define FALSE 0
#define TAG_SEND 2001
#define TAG_RETURN 2002

using namespace std;

typedef struct {
	int tiles[MAX_SIZE];
} State;

int N;

/*
 *	Function: printState
 * ----------------------------
 *	Prints the state of the board(0 - empty, 1 - queen)
 *
 *		state: state of the board 
 *	
 *		returns: nil 
 */
void printState(State state) {
	int row;
	for(row = 0; row < N; row++) {
		int col;
		for(col = 0; col < N; col++) {		 
			if (state.tiles[row] >> col & QUEEN)
				printf("1");
			else
				printf("0");
		}
		printf("\n");
	}
}

/*
 *	Function: attacks
 * ----------------------------
 *	Returns the number of attacks made by a particular queen
 *
 *		state: state of the board 
 *		row: row of the queen
 *		col: col of the queen
 *		wrap: 0 - no wraparound, 1 - wraparound
 *	
 *		returns: the number of attacks made by a particular queen 
 */
int attacks(State state, int row, int col, int wrap) {
	int i, attacks = 0, e, ne, n, nw, w, sw, s, se;
	e = ne = n = nw = w = sw = s = se = FALSE;
	
	//printf("this is [%d][%d]:\n", row, col);
	
	for(i = 1; i < N; i++) {
		int lessRow = row-i, moreRow = row+i, lessCol = col-i, moreCol = col+i;
		
		if(wrap) {
			if(lessRow < 0) lessRow+=N;
			if(lessCol < 0) lessCol+=N;

			moreRow %= N;
			moreCol %= N;
		}
		
		//printf("i:%d, %d, %d, %d, %d\n", i, lessRow, moreRow, lessCol, moreCol);
		
		if(lessCol >= 0) {
			//East
			if(!e && state.tiles[row] >> lessCol & QUEEN) {
				attacks++;
				e = TRUE;
				//printf("e[%d][%d], ", row, lessCol);
			}
			
			if(lessRow >= 0) {
				//Northeast
				if(!ne && state.tiles[lessRow] >> lessCol & QUEEN) {
					attacks++;
					ne = TRUE;
					//printf("ne[%d][%d], ", lessRow, lessCol);
				}
			}
			
			if(moreRow < N) {
				//Southeast
				if(!se && state.tiles[moreRow] >> lessCol & QUEEN) {
					attacks++;
					se = TRUE;
					//printf("se[%d][%d], ", moreRow, lessCol);
				}
			}
		}
		
		if(moreCol < N) {
			//West
			if(!w && state.tiles[row] >> moreCol & QUEEN) {
				attacks++;
				w = TRUE;
				//printf("w[%d][%d], ", row, moreCol);
			}
			
			if(lessRow >= 0) {
				//Northwest
				if(!nw && state.tiles[lessRow] >> moreCol & QUEEN) {
					attacks++;
					nw = TRUE;
					//printf("nw[%d][%d], ", lessRow, moreCol);
				}
			}
		
			if(moreRow < N) {
				//Southwest
				if(!sw && state.tiles[moreRow] >> moreCol & QUEEN) {
					attacks++;
					sw = TRUE;
					//printf("sw[%d][%d], ", moreRow, moreCol);
				}
			}
		}
		
		if(lessRow >= 0) {
			//North
			if(!n && state.tiles[lessRow] >> col & QUEEN) {
				attacks++;
				n = TRUE;
				//printf("n[%d][%d], ", lessRow, col);
			}
		}
		
		if(moreRow < N) {
			//South
			if(!s && state.tiles[moreRow] >> col & QUEEN) {
				attacks++;
				s = TRUE;
				//printf("s[%d][%d], ", moreRow, col);
			}
		}
		//printf("\n");
		
		if(n && ne && e && se && s && sw && w && nw) {
			return attacks;
		}
	}
	return attacks;
}

/*
 *	Function: computeKWithL
 * ----------------------------
 *	Returns the number of queens attacked by every single queen
 *
 *		state: state of the board 
 *		wrap: 0 - no wraparound, 1 - wraparound
 *	
 *		returns: the number of queens attacked by every single queen if it is exactly same, otherwise return -1 
 */
void computeK(State state, int wrap, int l, int *high, int *low, int *numQueens, vector<int> &queensList) {
	int currentRow, currentCol, firstQueen = TRUE;
	for(currentRow = 0; currentRow < N; currentRow++) {
		for(currentCol = 0; currentCol < N; currentCol++) {
			if(state.tiles[currentRow] >> currentCol & QUEEN) {
				if(firstQueen) {
					*high = *low = attacks(state, currentRow, currentCol, wrap);
					firstQueen = FALSE;
				} else {
					*low = min(*low, attacks(state, currentRow, currentCol, wrap));
					*high = max(*high, attacks(state, currentRow, currentCol, wrap));
				}
				*numQueens += 1;
				if(l) {
					queensList.push_back(currentRow * N + currentCol);
				}
			}
		}
	}
}

int generateMoreStates(State state, int k, int w, int l, int lastRow, vector<int> &resultsList) {
	int row = lastRow + 1, partialMax = 0, rowData;
	vector<int> partialQueensList;
	if(row == N) {
		return 0;
	}
	for(rowData = 0; rowData < (int)pow(2, N); rowData++) {
		int high = 0, low = 0, numQueens = 0;
		vector<int> queensList;
		State newState = state;
		newState.tiles[row] = rowData;
		
		computeK(newState, w, l, &high, &low, &numQueens, queensList);

		if(!(high > k)) {
			vector<int> someQueensList;
			int someQueens = generateMoreStates(newState, k, w, l, row, someQueensList);
			
			if(l) {
				if(someQueens == partialMax) {
					partialQueensList.insert(partialQueensList.end(), someQueensList.begin(), someQueensList.end());
				} else if(someQueens > partialMax) {
					partialQueensList.clear();
					partialQueensList.insert(partialQueensList.end(), someQueensList.begin(), someQueensList.end());
					partialMax = someQueens;
				}
			} else {
				partialMax = max(partialMax, someQueens);
			}
		}
		
		if(high == k && low == k) {
			if(l) {
				if(numQueens == partialMax) {
					partialQueensList.insert(partialQueensList.end(), queensList.begin(), queensList.end());
				} else if(numQueens > partialMax) {
					partialQueensList.clear();
					partialQueensList.insert(partialQueensList.end(), queensList.begin(), queensList.end());
					partialMax = numQueens;
				}
			} else {
				partialMax = max(partialMax, numQueens);
			}
		}
	}
	resultsList = partialQueensList;
	return partialMax;
}

int generateAndProcessState(int numProcesses, int processID, int k, int wrap, int l, vector<int> &resultsList) {
	int n = (int)pow(2, N);
	int statesPerProcess = n/numProcesses;
	int startIndex = processID * statesPerProcess;
	int i, partialMax = 0;
	vector<int> partialQueensList;
	for(i = startIndex; i < startIndex+statesPerProcess; i++) {
		vector<int> someQueensList;
		State state = {0};
		state.tiles[0] = i;
		int someQueens = generateMoreStates(state, k, wrap, l, 0, someQueensList);
		
		if(l) {
			if(someQueens == partialMax) {
				partialQueensList.insert(partialQueensList.end(), someQueensList.begin(), someQueensList.end());
			} else if(someQueens > partialMax) {
				partialQueensList.clear();
				partialQueensList.insert(partialQueensList.end(), someQueensList.begin(), someQueensList.end());
				partialMax = someQueens;
			}
		} else {
			partialMax = max(partialMax, someQueens);		
		}
	}
	resultsList = partialQueensList;
	return partialMax;
}

int main(int argc, char *argv[])
{
	if(argc < 5 || argc > 6) {
		printf("Incorrect amount of arguments. Please provide N k l w.");
	} else {
		N = atoi(argv[1]);
		int k = atoi(argv[2]);
		int l = atoi(argv[3]);
		int w = atoi(argv[4]);
		int h;
		if(argc == 6) {
			h = atoi(argv[5]);
		} else {
			h = 0;
		}
				
		int i, j, x, y, processID, numProcesses;
		int partialMax, maxQueens = 0;
		int listSize;
		MPI_Status status;
		
		//heuristics employed for special cases
		if(h) {
			if(w == 0 && k > 4) {
				maxQueens = 0;
				//output answer
				printf("%d,%d:%d:\n", N, k, maxQueens);
				return 0;
			}
			if(w && k == 8) {
				maxQueens = N * N;
				//output answer
				printf("%d,%d:%d:\n", N, k, maxQueens);
				return 0;
			}
		}


		/* starts MPI */
		MPI_Init (&argc, &argv);
		/* get current process id */
		MPI_Comm_rank(MPI_COMM_WORLD, &processID);
		/* get number of processes */
		MPI_Comm_size(MPI_COMM_WORLD, &numProcesses);
			
		if(processID == 0) {
			vector<int> fullQueensList;
			vector<int> someQueensList;
			
			//split and send data to slave processes
			for(i = 1;i < numProcesses;i++) {				
				MPI_Send(&numProcesses, 1 , MPI_INT, i, TAG_SEND, MPI_COMM_WORLD);
				MPI_Send(&k, 1 , MPI_INT, i, TAG_SEND, MPI_COMM_WORLD);
				MPI_Send(&w, 1 , MPI_INT, i, TAG_SEND, MPI_COMM_WORLD);
			}

			//do computation
			maxQueens = generateAndProcessState(numProcesses, 0, k, w, l, someQueensList);
			fullQueensList = someQueensList;
			
			//receive data from slave processes
			for(i = 1;i < numProcesses;i++) {
				MPI_Recv(&partialMax, 1, MPI_INT, i, TAG_RETURN, MPI_COMM_WORLD, &status);

				if(l) {
					MPI_Recv(&listSize, 1, MPI_INT, i, TAG_RETURN, MPI_COMM_WORLD, &status);
					vector<int> partialQueensList(listSize);
					MPI_Recv(&partialQueensList[0], listSize, MPI_INT, i, TAG_RETURN, MPI_COMM_WORLD, &status);
					
					if(partialMax == maxQueens) {
						fullQueensList.insert(fullQueensList.end(), partialQueensList.begin(), partialQueensList.end());
					} else if(partialMax > maxQueens) {
						fullQueensList.clear();
						fullQueensList.insert(fullQueensList.end(), partialQueensList.begin(), partialQueensList.end());
						maxQueens = partialMax;
					}
				} else {
					maxQueens = max(partialMax, maxQueens);
				}
			}

			//output answer
			printf("%d,%d:%d:", N, k, maxQueens);
			if(l) {
				for(j = 0; j < fullQueensList.size(); j++) {
					if(j%maxQueens == 0) {
						printf("{%d,", fullQueensList[j]);
					} else if(j%maxQueens == maxQueens - 1) {
						printf("%d}", fullQueensList[j]);
					} else {
						printf("%d,", fullQueensList[j]);
					}
				}
			}
			printf("\n");
		} else {
			//receive data from master process
			MPI_Recv(&numProcesses, 1, MPI_INT, 0, TAG_SEND, MPI_COMM_WORLD, &status);    
			MPI_Recv(&k, 1, MPI_INT, 0, TAG_SEND, MPI_COMM_WORLD, &status);    
			MPI_Recv(&w, 1, MPI_INT, 0, TAG_SEND, MPI_COMM_WORLD, &status);    


			vector<int> partialQueensList;
			partialMax = generateAndProcessState(numProcesses, processID, k, w, l, partialQueensList);
			listSize = partialQueensList.size();
			
			//return data to master process                                        
			MPI_Send(&partialMax, 1, MPI_INT, 0, TAG_RETURN, MPI_COMM_WORLD);
			if(l) {
				MPI_Send(&listSize, 1, MPI_INT, 0, TAG_RETURN, MPI_COMM_WORLD);
				MPI_Send(&partialQueensList[0], partialQueensList.size(), MPI_INT, 0, TAG_RETURN, MPI_COMM_WORLD);
			}
		}  
		MPI_Finalize();
	}	
	return 0;
}
