#ifndef VORONOI_H
#define VORONOI_H

typedef struct {
   int x, y; 
} Point2D;

extern "C" void cudaInitialize(int noPoints, int width, int height); 
extern "C" void cudaDeinitialize(); 
extern "C" void cpuVoronoiDiagram(Point2D *points, int *output, int noPoints, 
                          int width, int height); 
extern "C" void cudaVoronoiDiagram(Point2D *points, int *output, int noPoints, 
                           int width, int height, int mode); 

#endif
