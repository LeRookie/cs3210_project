#include "voronoi.h"

#define BLOCK_X		16
#define BLOCK_Y		16
#define TILE		(BLOCK_X * BLOCK_Y)

Point2D *d_points; 
int		*d_result; 

texture<int2> texPoints; 

// Kernels
#include "kernels.cu"

////////////////////////////////////////////////////////////////////////////////////////
// HELPER FUNCTIONS                                                                   //
////////////////////////////////////////////////////////////////////////////////////////

// We assume that width and height are divisible by BLOCK_X and BLOCK_Y respectively
void cudaVoronoiDiagram(Point2D *points, int *output, 
						int noPoints, int width, int height, int mode) 
{
	// Copy input to GPU
	cudaMemcpy(d_points, points, noPoints * sizeof(Point2D), cudaMemcpyHostToDevice); 

	// Compute the Voronoi diagram
	dim3 grid = dim3(width / BLOCK_X, height / BLOCK_Y); 
	dim3 block = dim3(BLOCK_X, BLOCK_Y); 

	switch (mode) {
		case 3: 
			kernelNaiveVoronoiDiagram<<< grid, block >>>(d_points, d_result, 
				noPoints, width); 
			break; 
		case 4: 
			kernelSharedVoronoiDiagram<<< grid, block >>>(d_points, d_result, 
				noPoints, width); 
			break; 
	}

	// Copy back the result
	cudaMemcpy(output, d_result, width * height * sizeof(int), cudaMemcpyDeviceToHost); 
}

void cudaInitialize(int noPoints, int width, int height) 
{
	// Allocate memory 
	cudaMalloc((void **) &d_points, noPoints * sizeof(Point2D)); 
	cudaMalloc((void **) &d_result, width * height * sizeof(int)); 
}

void cudaDeinitialize()
{
	cudaFree(d_points); 
	cudaFree(d_result); 
}