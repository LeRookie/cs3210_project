__device__ __host__ int distance(int x1, int y1, int x2, int y2) 
{
    return (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2); 
}

/* The CPU version of Voronoi Diagram computation */ 
void cpuVoronoiDiagram(Point2D *points, int *output, int noPoints, int width, int height)
{
    int id = 0; 

    for (int j = 0; j < height; j++) 
        for (int i = 0; i < width; i++) {
            int best = 0; 
            int minDist = distance(i, j, points[0].x, points[0].y);

            for (int k = 1; k < noPoints; k++) {
                int myDist = distance(i, j, points[k].x, points[k].y); 

                if (myDist < minDist) {
                    minDist = myDist; 
                    best = k;
                }
            }

            output[id++] = best; 
        }
}

/* GPU Version of Voronoi diagram computation 
 * Naive implementation
 */
__global__ void kernelNaiveVoronoiDiagram(Point2D *points, int *result, int noPoints, int width) 
{
    /************************
     * WRITE YOUR CODE HERE *
     ************************/
    // 1. Get the coordinate of this thread
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    int idx = x + y * width;
    // 2. Initialize

    int minDist = distance(x, y, points[0].x, points[0].y);
    int best = 0;

    // 3. Find the nearest site
    for (int i=1; i < noPoints; i++) {
        int d = distance(x, y, points[i].x, points[i].y);
        if (d < minDist) {
            best = i;
            minDist = d;
        }
    }
    // 4. Output the result
    result[idx] = best;
}

/* GPU Version of Voronoi diagram computation
 * Using shared memory
 * We assume for simplicity that noPoints is divisible by TILE
 */
__global__ void kernelSharedVoronoiDiagram(Point2D *points, int *result, int noPoints, int width) 
{
    /************************
     * WRITE YOUR CODE HERE *
     ************************/

    // 1. Get the coordinate of this thread
    int x = threadIdx.x + blockIdx.x * blockDim.x;
    int y = threadIdx.y + blockIdx.y * blockDim.y;
    int idx = x + y * width;
    int bIdx = threadIdx.x+threadIdx.y*blockDim.x;

    // 2. Initialize
    // Each time TILE points are loaded
    __shared__ Point2D spoints[TILE];
    int minDist = distance(x, y, points[0].x, points[0].y);
    int best = 0;

    // 3. Find the nearest site
    int readBlock = 0;
    for (;readBlock < noPoints / TILE; readBlock++) {

        // 3.1 Each thread read one point into the shared array
        spoints[bIdx] = points[bIdx + TILE * readBlock];
        // 3.2 Make sure everyone has finished reading
        __syncthreads();

        // 3.3 Check with all points in the shared array
        for (int i=0; i<TILE; i++) {
            Point2D p = spoints[(i+bIdx)%TILE];
            int d = distance(x, y, p.x, p.y);
            if (d < minDist) {
                best = (i+bIdx)%TILE + TILE * readBlock;
                minDist = d;
            }
        }
        // 3.4 Make sure everyone has finished using the shared array
        __syncthreads();
    }

    // 4. Output the result
    result[idx]=best;
}

