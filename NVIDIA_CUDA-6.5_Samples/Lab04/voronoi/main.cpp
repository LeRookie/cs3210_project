#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <GL/glew.h>
#if defined(WIN32)
#include <GL/wglew.h>
#endif                /* */
#include <GL/glut.h>
#include <cuda.h>
// Utilities and system includes
#include <assert.h>
#include <helper_string.h>  // helper for shared functions common to CUDA Samples

// CUDA runtime
#include <cuda_runtime.h>
#include <cublas_v2.h>

// CUDA and CUBLAS functions
#include <helper_functions.h>
#include <helper_cuda.h>



//#include <cutil_inline.h>
//#include <cutil_gl_inline.h>
#include "voronoi.h"

// **********************************************************************************************
//*Global Vars
// **********************************************************************************************

int noPoints = 256;            //Assume divisible by 256
int fboWidth = 800;            //Assume divisible by 16
int fboHeight = 608;

int mode = 1;                  //1:Sites; 2:CPU; 3:GPU - naive; 4:GPU - shared; 5:GPU, Texture fetching
bool animation = false;        //Sites moving

Point2D * points;
int *result;
GLubyte *outputTexture;
GLubyte *colorMap;
GLuint texture;
Point2D *newPoints;
double speed = 2.0;
float scale = 1.0, transX = 0.0, transY = 0.0;
bool isLeftMouseActive = false, isRightMouseActive = false;
int oldMouseX = 0, oldMouseY = 0;
int frames = 0;
// int updateFreq = 3;       //Update FPS every 3 seconds
double fps = 0.0;
StopWatchInterface *timer = NULL;

static int fpsCount = 0;
static int fpsLimit = 1;
cudaEvent_t startEvent, stopEvent;
void *font = GLUT_BITMAP_HELVETICA_18;

/**********************************************************************************************
 * INITIALIZATION
 **********************************************************************************************/

void generatePoints(Point2D * points, int noPoints, int fboWidth, int fboHeight)
{
    for (int i = 0; i < noPoints; i++) {
    points[i].x = rand() % fboWidth;
    points[i].y = rand() % fboHeight;
    }
}

void initialization()
{
    //Allocate memory
    points = (Point2D *) malloc(noPoints * sizeof(Point2D));
    newPoints = (Point2D *) malloc(noPoints * sizeof(Point2D));
    result = (int *) malloc(fboWidth * fboHeight * sizeof(int));
    outputTexture = (GLubyte *) malloc(fboWidth * fboHeight * 3 * sizeof(GLubyte));
    colorMap = (GLubyte *) malloc(noPoints * 3 * sizeof(GLubyte));
    //Generate random points
    generatePoints(points, noPoints, fboWidth, fboHeight);
    //Also generate the color for each site
    int id = 0;
    for (int i = 0; i < noPoints; i++, id += 3) {
    colorMap[id + 0] = rand() & 0xFF;
    colorMap[id + 1] = rand() & 0xFF;
    colorMap[id + 2] = rand() & 0xFF;
    }
    //Initialize the output texture
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
    //Initialize CUDA
    cudaInitialize(noPoints, fboWidth, fboHeight);
    //Print out some instruction
    printf("Image size: %i x %i\n", fboWidth, fboHeight);
    printf("Number of points: %i\n", noPoints);
    printf("\n");
    printf(" Left click  : Move the image\n");
    printf(" Right click : Zoom\n");
    printf("\n");
    printf(" Press '1'   : Display the input points\n");
    printf(" Press '2'   : Use CPU to compute the Voronoi Diagram\n");
    printf(" Press '3'   : Use GPU naively to compute the Voronoi Diagram\n");
    printf(" Press '4'   : Use GPU with shared memory to compute the Voronoi Diagram\n");
    printf("\n");
    printf(" Press 'space' to animate the points\n");
}

/**********************************************************************************************
 * DEINITIALIZATION
 **********************************************************************************************/

void deinitialization()
{
    glDeleteTextures(1, &texture);
    free(points);
    free(result);
    free(colorMap);
    free(outputTexture);
    cudaDeinitialize();
}

// This function computes the Voronoi diagram using different algorithms.
// The result is then rendered into a texture

void computeVoronoiDiagram()
{
    switch (mode) {
    case 1:
    break;
    case 2:
    cpuVoronoiDiagram(points, result, noPoints, fboWidth, fboHeight);
    break;
    case 3:
    case 4:
    case 5:
    cudaVoronoiDiagram(points, result, noPoints, fboWidth, fboHeight, mode);
    break;
    default:
    return;
    }
    //Generate the texture
    int q = 0, r;
    for (int p = 0; p < fboWidth * fboHeight; p++, q += 3)
    if (result[p] < 0 || result[p] >= noPoints) {
        outputTexture[q + 0] = 0;
        outputTexture[q + 1] = 0;
        outputTexture[q + 2] = 0;
    } else {
        r = result[p] * 3;
        outputTexture[q + 0] = colorMap[r + 0];
        outputTexture[q + 1] = colorMap[r + 1];
        outputTexture[q + 2] = colorMap[r + 2];
    }
    //Copy the result to a texture for rendering
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
    glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGB8, fboWidth, fboHeight, 0, GL_RGB,
         GL_UNSIGNED_BYTE, outputTexture);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
}

void glut_mouse(int button, int state, int x, int y)
{
    if (state == GLUT_UP)
    switch (button) {
    case GLUT_LEFT_BUTTON:
        isLeftMouseActive = false;
        break;
    case GLUT_RIGHT_BUTTON:
        isRightMouseActive = false;
        break;
    }
    if (state == GLUT_DOWN) {
    oldMouseX = x;
    oldMouseY = y;
    switch (button) {
    case GLUT_LEFT_BUTTON:
        isLeftMouseActive = true;
        break;
        break;
    case GLUT_RIGHT_BUTTON:
        isRightMouseActive = true;
        break;
        break;
    }
    }
}


void glut_mouseMotion(int x, int y)
{
    if (isLeftMouseActive) {
    transX += 2.0 * double (x - oldMouseX) / scale / fboWidth;
    transY -= 2.0 * double (y - oldMouseY) / scale / fboHeight;
    glutPostRedisplay();
    } else if (isRightMouseActive) {
    scale -= (y - oldMouseY) * scale / 400.0;
    glutPostRedisplay();
    }
    oldMouseX = x;
    oldMouseY = y;
}

void drawPoints()
{
    glColor3d(1.0, 1.0, 1.0);
    glPointSize(3.0);
    glBegin(GL_POINTS);
    for (int i = 0; i < noPoints; i++)
    glVertex2d(double (points[i].x) / fboWidth,
           double (points[i].y) / fboHeight);
    glEnd();
}

void renderString(char *str)
{
    while (*str != '\0') {
    glutBitmapCharacter(font, *str++);
    }
}


void glutDisplay()
{
    float milliseconds = 1;
    //Initialization
    glViewport(0, 0, (GLsizei) fboWidth, (GLsizei) fboHeight);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    //Setup projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0, 1.0, 0.0, 1.0);
    //Setup modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glScalef(scale, scale, 1.0);
    glTranslatef(transX, transY, 0.0);
    //Setting up lights
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    //Draw the texture
    // Do not draw texture when we are in mode 1
    if (mode > 1) {
    glEnable(GL_TEXTURE_RECTANGLE_ARB);
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, texture);
    glColor3d(1.0, 1.0, 1.0);
    glBegin(GL_QUADS);
    glTexCoord2i(0, fboHeight);
    glVertex2f(0.0, 1.0);
    glTexCoord2i(fboWidth, fboHeight);
    glVertex2f(1.0, 1.0);
    glTexCoord2i(fboWidth, 0);
    glVertex2f(1.0, 0.0);
    glTexCoord2i(0, 0);
    glVertex2f(0.0, 0.0);
    glEnd();
    glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
    glDisable(GL_TEXTURE_RECTANGLE_ARB);
    }
    drawPoints();
    fpsCount++;
    //stop timer
    if (mode == 2) {
    //ie it is the CPU
    milliseconds = sdkGetTimerValue(&timer);
    sdkResetTimer(&timer);
    } else {
    checkCudaErrors(cudaEventRecord(stopEvent, 0));
    checkCudaErrors(cudaEventSynchronize(stopEvent));
    checkCudaErrors(cudaEventElapsedTime(&milliseconds, startEvent, stopEvent));
    }
    if (animation) {
    char buffer[20];
    milliseconds /= (float) fpsCount;
    fps = 1.f / (milliseconds / 1000.f);
    sprintf(buffer, "%.2f FPS", fps);
    glColor3f(0.0f, 0.0f, 0.0f);
    double blockWidth = glutBitmapLength(font, (const unsigned char *) buffer) + 4.0;
    double blockHeight = 20.0;
    glBegin(GL_QUADS);
    glVertex2f(0.0, 0.0);
    glVertex2f(0.0, blockHeight / fboHeight);
    glVertex2f(blockWidth / fboWidth, blockHeight / fboHeight);
    glVertex2f(blockWidth / fboWidth, 0.0);
    glEnd();
    glColor3f(1.0f, 1.0f, 1.0f);
    glRasterPos2f(2.0f / fboWidth, 2.0f / fboHeight);
    renderString(buffer);
    if (fpsCount >= fpsLimit) {
        fpsCount = 0;
    }
    }
    if (mode == 2) {
    milliseconds = sdkGetTimerValue(&timer);
    sdkResetTimer(&timer);
    } else {
    checkCudaErrors(cudaEventRecord(startEvent, 0));
    }
    glutSwapBuffers();
}

void movePoints(Point2D * source, Point2D * dest, int noPoints)
{
    double dx, dy, length;
    for (int i = 0; i < noPoints; i++) {
    while (true) {
        dx = dest[i].x - source[i].x;
        dy = dest[i].y - source[i].y;
        length = sqrt(dx * dx + dy * dy);
        if (length >= speed)
        break;
        //Too close, pick a new destination
        dest[i].x = rand() % fboWidth;
        dest[i].y = rand() % fboHeight;
    }
    source[i].x += (int) (dx / length * speed);
    source[i].y += (int) (dy / length * speed);
    }
}

void glut_idle()
{
    if (animation) {
    movePoints(points, newPoints, noPoints);
    computeVoronoiDiagram();
    glutPostRedisplay();
    frames++;
    }
}

void glut_keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case '1':
    mode = 1;
    break;
    case '2':
    mode = 2;
    printf("2. Using CPU to compute Voronoi Diagram\n");
    computeVoronoiDiagram();
    break;
    case '3':
    mode = 3;
    printf("3. Using GPU Naive algorithm \n");
    computeVoronoiDiagram();
    break;
    case '4':
    mode = 4;
    printf("4. Using GPU with shared memory \n");
    computeVoronoiDiagram();
    break;
    case ' ':
    animation = !animation;
    if (animation) {
        frames = 0;
        generatePoints(newPoints, noPoints, fboWidth, fboHeight);
    }
    break;
    }
    glutPostRedisplay();
}


int main(int argc, char **argv)
{
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(fboWidth, fboHeight);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInit(&argc, argv);
    glutCreateWindow("Voronoi Diagram");
    initialization();
    glutDisplayFunc(glutDisplay);
    glutMouseFunc(glut_mouse);
    glutMotionFunc(glut_mouseMotion);
    glutKeyboardFunc(glut_keyboard);
    glutIdleFunc(glut_idle);
    //Start the program
    sdkCreateTimer(&timer);
    sdkStartTimer(&timer);
    checkCudaErrors(cudaEventCreate(&startEvent));
    checkCudaErrors(cudaEventCreate(&stopEvent));
    checkCudaErrors(cudaEventRecord(startEvent, 0));
    glutMainLoop();
    //Release resources
    deinitialization();
    return 0;
}
