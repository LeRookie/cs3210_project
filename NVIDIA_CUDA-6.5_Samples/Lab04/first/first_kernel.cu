
//#include <stdlib.h>
#include <stdio.h>
//#include <string.h>
//#include <math.h>
#include <sys/time.h>

#include <cuda.h>

#define NBLOCKS (long)32
#define NTHREADS (long)256
#define NELEMENTS (long)10240

__device__ void block_reduce(int * d_x)
{
    int t;
    for (int d = 1; d < blockDim.x; d *= 2)
    {
	if (threadIdx.x % (2 * d) == 0)
	{
	    t = d_x[threadIdx.x + d];
	    d_x[threadIdx.x] += t; 
	}
	__syncthreads();
    }
}

/* kernel routine */
__global__ void my_first_kernel(int *d_a, int *d_b, int * d_res)
{
    int i, idx = blockIdx.x * blockDim.x + threadIdx.x;
    __shared__ int thread_results[NTHREADS];
    
    thread_results[threadIdx.x] = 0;
    /* Each thread computes the product of its own elements */
    for (i = idx; i < idx + NELEMENTS; i++)
    {
	thread_results[threadIdx.x] += d_a[i] * d_b[i];
    }
    /* Wait for all threads in the block to finish */
    __syncthreads();
    
    /* Now each thread has wrote its product 
       in thread_results[threadIdx] */
    block_reduce(thread_results);
    
    if (threadIdx.x == 0)
	atomicAdd(d_res, thread_results[0]);  

}

void init()
{
    /* Host arrays */
    int *h_a, *h_b, h_c; 
    /* Device arrays */
    int *d_a, *d_b, *d_c;
    struct timeval before, after;
    dim3 dimBlock, dimGrid;
    
    int i;
    long nsize;	
	
    /* set number of blocks, and threads per block */
    nsize = NBLOCKS*NTHREADS*NELEMENTS;
    
    printf("nblocks=%ld\n", NBLOCKS);
    printf("nthreads=%ld\n", NTHREADS);
    printf("perthread=%ld\n", NELEMENTS);

    /* allocate memory for host arrays */
    h_a = (int *)malloc(nsize*sizeof(int));
    h_b = (int *)malloc(nsize*sizeof(int));
	
	
    /* allocate memory for device arrays*/
    cudaMalloc((void **)&d_a, nsize*sizeof(int));
    cudaMalloc((void **)&d_b, nsize*sizeof(int));
    cudaMalloc((void **)&d_c, sizeof(int));

    /* initialize host arrays */
    srand(0);
    
    for ( i = 0; i < nsize; i++)
    {
        h_a[i] = (int) i;
        h_b[i] = (int) i;
    }
    h_c = 0;

    /* copy data from host to dest */
    cudaMemcpy(d_a, h_a, nsize * sizeof(int), cudaMemcpyHostToDevice); 
    cudaMemcpy(d_b, h_b, nsize * sizeof(int), cudaMemcpyHostToDevice); 
    cudaMemcpy(d_c, &h_c, sizeof(int), cudaMemcpyHostToDevice);
    
    //printf("a=(");
    //for ( i = 0; i < nsize; i++)
    //	printf(" %03d", h_a[i]);
    //printf(")\n");
    
    //printf("b=(");
    //for ( i = 0; i < nsize; i++)
    //	printf(" %03d", h_b[i]);
    //printf(")\n");
    
    dimGrid.x = NBLOCKS;
    dimGrid.y = 1;
    dimGrid.z = 1;
    
    dimBlock.x = NTHREADS;
    dimBlock.y = 1;
    dimBlock.z = 1;
    
    /* execute kernel */
   gettimeofday(&before, NULL);
    my_first_kernel<<<dimGrid,dimBlock>>>(d_a, d_b, d_c);
   gettimeofday(&after, NULL);
    
   printf("Time: %ld\n", (long)((after.tv_sec-before.tv_sec) * 1000000 + (after.tv_usec - before.tv_usec)) );

    /* copy back results */
    cudaMemcpy(&h_c, d_c, sizeof(int), cudaMemcpyDeviceToHost);

    /* print results */
    printf("Product = %d\n", h_c);

    /* free memory */
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
    free(h_a);
    free(h_b);

}
