CC=mpic++
# CC=gcc

.phony:
	proj

proj: proj.cpp
	$(CC) -o proj proj.cpp

profile: proj.cpp
	$(CC) -o proj proj.cpp -pg

test:
	mpirun -np 4 proj 4 0 0 0

clean:
	rm proj
