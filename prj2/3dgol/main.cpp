#include <algorithm>
#include <string>
#include <sstream>
#include <climits>
#include <iostream>
#include <fstream>
#include "3dgol.h"

#define NOFILE ""

using namespace std;

int dp, sz, r1, r2, r3, r4, sp, gen;
string fl;
mylong *h_field;

// Assume that the sz is less than 64
// Array is structured as such
// [(0,0,0), (0,0,1), .. (0,0,sz-1), (0,1,0)..(0,sz-1,sz-1),(1,0,0) ..]
// (z, x, y)

void readFieldFromFile(string filename, mylong *h_field) {
  ifstream myfile(filename, ios::in);

  if (myfile.fail()) {
    cerr << "[ERROR] Unable to open file at " << filename << endl;
    exit(1);
  }

  string line;
  for (int z=0; z<sz; z++) {
    for (int x=0; x<sz && myfile.good() && !myfile.eof(); x++) {
      getline(myfile, line);

      if (line == "-1" || myfile.eof()) {
        break;
      } else {
        for (int y=0; y<int(line.length()/2)+1; y++) {
          char alive = line[y*2];
          if (alive == '1') {
            h_field[x+y*sz+z*sz*sz] = 1;
          }
        }
      }
    }
  }

  myfile.close();
}

void writeFieldToFile(mylong *h_field) {
  string filename = "final.txt";
  ofstream myfile(filename);
  if (myfile.is_open()) {
    
	for (int z=0; z<sz; z++) {
      for (int x=0; x<sz; x++) {
        for (int y=0; y<sz; y++) {
          mylong cell = h_field[x+y*sz+z*sz*sz];
		  if(y == sz-1) {
		    myfile << cell;
		  } else {
            myfile << cell << " ";
		  }
        }
        myfile << endl;
      }
	  myfile << "-1" << endl;
    }
	
    myfile.close();
  } else {
    cerr << "[ERROR] Unable to write to file at " << filename << endl;
  }
}

void printField(mylong *h_field) {
  for (int z=0; z<sz; z++) {
    for (int x=0; x<sz; x++) {
      for (int y=0; y<sz; y++) {
        mylong cell = h_field[x+y*sz+z*sz*sz];
        cout << cell << " ";
      }
      cout << endl;
    }
	cout << "-1" << endl;
  }
}

void preprocess(int argc, char **argv) {
  // Read in the parameters
  dp = atoi(argv[1]); // displays the field every n generation
  sz = atoi(argv[2]); // Size of game sz by sz by sz
  r1 = atoi(argv[3]); // new cell if r1 <= n <= r2
  r2 = atoi(argv[4]);
  r3 = atoi(argv[5]); // die if r3 < n < r4
  r4 = atoi(argv[6]);
  sp = atoi(argv[7]); // sp = 1000 ==> 1 gen in 1 second, 0 ==> afap
  gen = atoi(argv[8]); // generations to run

  fl = NOFILE;

  h_field = new mylong[sz * sz * sz];

  // Initialise field to all zeros
  for (int z=0; z<sz; z++) {
    for (int x=0; x<sz; x++) {
      for (int y=0; y<sz; y++) {
        h_field[x+y*sz+z*sz*sz] = 0;
      }
    }
  }

  // Generate/Read in the field
  if (argc == 10) {
    fl = string(argv[9]);
    readFieldFromFile(fl, h_field);
  } else {
    for (int z=0; z<sz; z++) {
      for (int x=0; x<sz; x++) {
        for (int y=0; y<sz; y++) {
          h_field[x+y*sz+z*sz*sz] = rand()>0.5;
        }
      }
    }
  }
}

void postprocess() {
  // Free memory
}

int main(int argc, char **argv) {
  if(argc < 9 || argc > 10) {
    cout << "Usage: ./3dgol dp sz r1 r2 r3 r4 sp gen [fl]" << endl;
    exit(1);
  }
  
  preprocess(argc, argv);

  //cout << "Size : " << sz << ", Generations : " << gen << endl;
  //cout << "dp : " << dp << ", live : " << r1 << " - " << r2 << endl;
  //cout << "die : " << r4 << " - " << r3 << endl;

  if (dp != 0) {
    // Run forever if gen==0
    if(gen == 0) {
      while(true) {
        kernel_wrapper(h_field, dp, r1, r2, r3, r4, sz, sp);
        printField(h_field);
      }
    } else {
    // Else run dp steps at a time
      int steps = min(gen, dp);
      while (gen > 0) {
        kernel_wrapper(h_field, steps, r1, r2, r3, r4, sz, sp);
        gen -= dp;
        printField(h_field);
      }
    }
  } else {
    // Run forever if gen==0
    if(gen == 0) {
      while(true) {
        kernel_wrapper(h_field, 1, r1, r2, r3, r4, sz, sp);
      }
      printField(h_field);
    } else {
      kernel_wrapper(h_field, gen, r1, r2, r3, r4, sz, sp);
      printField(h_field);
    }
  }
  
  // Write the field to outFile
  writeFieldToFile(h_field);

  postprocess();
  return 0;
}
