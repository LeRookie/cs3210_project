#ifndef D3GOLH
#define D3GOLH
#define mylong short
#define NUM_BITS 64

void cudaPreprocess(mylong *d_field_in, mylong *d_field_out, int sz);
void cudaPostprocess(mylong *d_field_in, mylong *d_field_out);

extern "C"
void kernel_wrapper(mylong *h_field, int steps, int r1, int r2, int r3, int r4, int sz, int sp);

#endif
