#include "3dgol.h"
#include <climits>
#include <iostream>

__global__ void step(mylong *out, mylong *in, int steps,
    int r1, int r2, int r3, int r4, int sz) {

  int x, y, z, position;
  x = threadIdx.x + blockIdx.x * gridDim.x;
  y = threadIdx.y + blockIdx.y * gridDim.y;
  z = threadIdx.z + blockIdx.z * gridDim.z;
  if (x >= sz || y >= sz || z >= sz) {
    return;
  }

  position = x + y * sz + z * sz * sz;

  int curr = in[position];
  int alive = 0;

  // Compute number of living neighbours
  
  for (int iz = -1; iz < 2; iz++) {
    for (int ix = -1; ix < 2; ix++) {
      for (int iy = -1; iy < 2; iy++) {
        // Skip itself
	if(iz==0 && ix==0 && iy==0) {
	  continue;
	}
	
	// Take care of edge cases	
	int nz, nx, ny;
        if(z+iz<0) {
	  nz = sz-1;
	} else if(z+iz>sz-1) {
	  nz = 0;
	} else {
	  nz = z + iz;
	}
	if(x+ix<0) {
	  nx = sz-1;
	} else if(x+ix>sz-1) {
	  nx = 0;
	} else {
	  nx = x + ix;
	}
	if(y+iy<0) {
	  ny = sz-1;
	} else if(y+iy>sz-1) {
	  ny = 0;
	} else {
	  ny = y + iy;
	}
        int s = in[nx+ny*sz+nz*sz*sz];
        alive += s;
      }
    }
  }

  // Determine whether to stay alive or to die
  if ((curr == 0 && r1 <= alive && alive <= r2) || (curr == 1 && r3 < alive && alive < r4)) {
    out[position] = 1-curr;
  } else {
    out[position] = curr;
  }
}

void cudaPreprocess(mylong *d_field_in, mylong *d_field_out, int sz) {
  // Initialize the CUDA arrays
  cudaMalloc(&d_field_in, sizeof(mylong) * sz * sz * sz);
  cudaMalloc(&d_field_out, sizeof(mylong) * sz * sz * sz);

  // Zero the output array
  cudaMemset(d_field_out, 0, sizeof(mylong) * sz * sz * sz);
}

void cudaPostprocess(mylong *d_field_in, mylong *d_field_out) {
  cudaFree(d_field_in);
  cudaFree(d_field_out);
}

void kernel_wrapper(mylong *h_field, int steps, int r1, int r2,
    int r3, int r4, int sz, int sp) {
  mylong *d_field_in;
  mylong *d_field_out;
  int cpySize = sizeof(mylong) * sz * sz * sz;

  // Initialize the CUDA arrays
  cudaMalloc(&d_field_in, sizeof(mylong) * sz * sz * sz);
  cudaMalloc(&d_field_out, sizeof(mylong) * sz * sz * sz);

  // Zero the output array
  cudaMemset(d_field_out, 0, sizeof(mylong) * sz * sz * sz);

  //cudaPreprocess(d_field_in, d_field_out, sz);

  // Copy the data over
  cudaMemcpy(d_field_in, h_field, cpySize, cudaMemcpyHostToDevice);

  // int MAX_THREADS_PER_BLOCK = 1024;
  int THREADS_PER_BLOCK = 4;
  int g = (int)ceil(float(sz)/THREADS_PER_BLOCK);
  dim3 grid(g, g, g);
  dim3 block(THREADS_PER_BLOCK, THREADS_PER_BLOCK, THREADS_PER_BLOCK);
  //std::cout << "Grid size : " << g << ", blocksize : " << block.x << std::endl;

  cudaEvent_t start, stop;
  float time;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);

  cudaEventRecord(start, 0);

  for (int i=0; i<steps; i++) {
    time = 0;
    step <<< grid, block >>>(d_field_out, d_field_in, steps, r1, r2, r3, r4, sz);
    std::swap(d_field_in, d_field_out);
    
    while(time < sp) {
      cudaEventRecord(stop, 0);
      cudaEventSynchronize(stop);
      cudaEventElapsedTime(&time, start, stop);
    }
  }

  if (steps % 2 == 1) {
    cudaMemcpy(d_field_out, d_field_in, cpySize, cudaMemcpyDeviceToDevice);
  }

  cudaError_t err = cudaGetLastError();
  if (err != cudaSuccess) {
    std::cerr << "[ERROR] " << cudaGetErrorString(err) << std::endl;
  }

  cudaMemcpy(h_field, d_field_out, cpySize, cudaMemcpyDeviceToHost);

  cudaFree(d_field_in);
  cudaFree(d_field_out);
}
